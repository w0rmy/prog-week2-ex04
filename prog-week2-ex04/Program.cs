﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace loop
{
    class Program
    {
        static void Main(string[] args)
        {

        var counter = 5;
        var i = 0;

        //for (i = 0; i < counter; i++)
        for (; i < counter; i++) //Query Jeff about how this works and why we define i in his above example.
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
        }
    }
}
